# Propostes sobre Programari Lliure

## Objectius

Tres nivells:

  1. Valorar les solucions lliures per les necessitats ja establertes
  2. Utilitzar exclusivament programari lliure a l'administració
  3. Promoure l'ús de programari lliure a la societat

Objectius concrets:

  * (1.a) Evitar cap obstacle en l'ús de programari lliure per part de la ciutadania, facilitar-ho quan sigui possible.
  * (1.b) No forçar la ciutadania a utilitzar solucions que d'un proveïdor concret. Especialment quan es tracta de serveis que recol·lecten dades.
  * (2.a) Assegurar que tot el codi finançat per la ciutadania estigui a disponible.
  * (2.b) Ple control i capacitat de decisió sobre quines tecnologies s'implanten, qui en gestiona les dades i quins algorismes les governen.
  * (2.c) Adoptar solucions que tinguin en compte l'obsolescència dels productes implantats, per minimitzar els residus electrònics.
  * (2.d) Compartir coneixement i costos entre ajuntaments i entitats de l'economia social a l'hora d'implantar sistemes informàtics. No s'ha de partir de zero en cada projecte.
  * (2.e) Potenciar l'ús de programari lliure per part dels ajuntaments.
  * (3.a) Promoure la innovació i la creació d'un teixit tecnològic local basat en tecnologies lliures
  * (3.b) Invertir recursos públics en mantenir, cuidar i fer créixer el patrimoni digital.

## Proposta programàtica

  * Les webs, aplicacions i plataformes tecnològiques municipals, han de funcionar correctament sobre plataformes lliures (navegadors, escriptoris, sistemes operatius), sense requerir la instal·lació components que no siguin lliures. Una bona manera és respectant els estàndards de la indústria (W3C, IEEE, etc.), tot i que no la única.
  * Basar els nous desenvolupaments en tecnologies lliures. Publicar-los sota una llicència de programari lliure. En cas que no sigui possible, caldrà aïllar aquelles parts que transitòriament hagin de fer ús de tecnologies privatives en forma de components substituïbles amb interfícies ben documentades. TODO: desenvolupaments tecnològics i estudis tècnics en general (Muriel).
  * La compra de maquinari ha de tenir com a prioritat que aquest pugui funcionar sense inconvenients sobre programari lliure. Es valorarà positivament que aquest maquinari estigui sota el control de l'usuari, que no tingui parts que l'usuari no pot saber com funcionen i no pugui reemplaçar.
  * Quan es publiqui un projecte, caldrà fer-ho de manera que qualsevol persona amb els coneixements tècnics necessaris el pugui reutilitzar.
  * Quan es modifiqui un projecte, caldrà contribuir les millores i modificacions realitzades al projecte original, procurant que siguin incloses abans de fer-les servir (upstream first).
  * Respectar les pràctiques i els codis de conducta, explícits o no, de les comunitats en que es participa.
  * Crear mecanismes de coordinació entre administracions i altres entitats que permetin mancomunar desenvolupaments. Fomentar la conversa prèvia amb organitzacions interessades per incloure-les en processos de disseny i desenvolupament que resolguin necessitats comunes.
  * Formar el personal administratiu en programari lliure.
  * Tenir en compte que tot el que es vulgui fer amb programari lliure probablement ja s'ha fet abans. Tenir en compte projectes que ja es fan servir arreu del món com Linux, Libre Office.

### Experiències inspiradores

  * Hi ha multitud d'organitzacions fent servir Linux i programari lliure (llista no actualitzada): https://en.wikipedia.org/wiki/List_of_Linux_adopters
  * Munic: https://www.kdab.com/the-limux-desktop-and-the-city-of-munich/ (beneficis guanyats amb coses que ha fet aquesta consultora). TODO: mirar com presentar-ho, és un cas controvertit.
  * TODO: UK libreoffice, Austràlia, Nova Zelanda.
  * Projecte Decidim.org: s'utilitza ja a molts municipis
  * Migració a programari lliure de l'Ajuntament de Saragossa

### Referències

  * Campanya Public Money, Public Code: https://publiccode.eu/ca/
  * Projecte GNU: https://www.gnu.org/philosophy/government-free-software.en.html
  * Barcelona: https://ajuntament.barcelona.cat/digital/ca/documentacio

==== Justificació dels objectius i la proposta programàtica ====

TODO.
