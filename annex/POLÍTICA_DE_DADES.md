# Propostes references a Política de Dades

## Open data per defecte

**Objectius**
  * Fer creixer les col·leccions de dades de lliure accés i facil explotació.
  * Fer més transparent l'adminstració.

**Proposta programàtica**
  * Publicar per defecte sota llicencies lliures i formats explotables tota la informació pública generada o gestionada per les entitatats municipals. Des del pressupostos municipals fins als temps dels semàfors
  * Les dades generades per a la realització d'estudis finançats amb diners públics també hauran de ser publicades sota llicencies lliures i formats explotables. De tal manera que es puguin comprovar els estudis o fer-ne de derivats.

### a tenir en compte
* s'ha d'insitir en la qualitat de les dades i no tant en la quantitat. Amb temes de transparencia això és especialment important, les dades agregades també poden ser molt útils.
* s'ha de fer alguna restricció en l'us de les dades? S'ha de fer pagar alguna cosa al agunes empreses?
* facilitar la federació dels diferents datasets per poder fer cerques sobre tots alhora.
* l'ajuntament hauria de treballar directament amb les dades publicades.


**Experiències inspiradores**



## Privacitat de les dades de caràcter personal

**Objectius**
  * Garantir la privacitat de les dades de caràcter personal.
  * Adminstrar les dades personals de manera més conscient, reduint els riscos derivats de la seva explotació.
  * Establir criteris ètics per a la explotació de les dades de caràcter personal.

**Proposta programàtica**
  * Administrar les dades de caràcter personal únicament dins de l'administració i seguint estrictes criteris de seguretat. Garantint els mitjans tècnics i els coneixements per gestionar aquest tipus de dades sense dependre de tercers.
  * Recollir dades de caracter personal només si previament s'han establert les finalitats concretes per a les que seran utilitzades. Minimitzant la quantitat de dades personals que es recullen. Si es recullen dades per entendre com es mouen les persones dins d'una ciutat i poder així millorar el transport públic, aquestes dades no es podran utilitzar per estudiar els seus habits de consum.

**Experiències inspiradores**
  * catsalut
  * comunitat europea
  * https://www.decode.com/
