# Propostes references a Infrastructura de Telecomunicacions

## Desplegament de nova infrastructura

**Objectius**
  * Tendir cap a la universalització i sobirania de les xarxes de telecomunicacions.

**Proposta programàtica**
  * Proporcionar accés real a la ciutadania i a la societat en general a una oferta assequible i variada de serveis de telecomunicacions de la màxima qualitat, capacitat i neutrals.
  * Promoure ordenances municipals de deplegament de nova infrastructura com la proposada per Guifi.net.
  * Afavorir les xarxes comunals i obertes.

**Experiències inspiradores**
  * Guifi.net: https://fundacio.guifi.net/ca_ES/
  * Xarxa Oberta de Catalunya: https://www.xarxaoberta.cat/

**Males pràctiques**
  * Free Basic: https://info.internet.org/es/

## Internet dels Objectes
**Objectius**
  * Construcció d’una xarxa de dades d’Internet de les coses oberta, lliure, neutral creada col·lectivament des de baix.

**Proposta programàtica**
  * Promoure les iniciatives de xarxes internet dels objectes oberta, lliure i neutral.
  * Contribuïr a una arquitectura de internet dels objectes per les ciutats que sigui oberta i interoperable.

**Experiències inspiradores**
  * TTNcat: https://thethingsnetwork.cat/index.php/The_Things_Network_Catalunya  
  * SENTILO: http://www.sentilo.io/wordpress/new-sentilo-release-1-5-1-is-now-multi-tenant/

## Desplegament de la xarxa mòbil de cinquena generació
**Objectius**
  * Impulsar que el desplegament del 5G es faci a través d'una xarxa compartida pels diferents operadors.

**Proposta programàtica**
  * Impulsar que el desplegament del 5G es faci a través d'una xarxa compartida pels diferents operadors.
  * Facilitat la implantació d'antenes en edificis públics.

**Experiències inspiradores**
  * 5GCity: https://www.5gcity.eu/

## Contractació de serveis de telecomunicacions
**Objectius**
  * Potenciar les operadores de proximitat. Evitant el monopoli de les grans operadores.
  * Potenciar l'ús neutre de les xarxes de telecomunicacions.
  * Major control públic dels operadors.

**Proposta programàtica**
  * No contractar serveis a companyies de telecomunicacions que discriminin el trafic, el filtrin o l'interrompin. Un exemple de tràfic filtrat i blocat és el relacionat amb el referendum d'autodeterminació de l'1 d'Octubre de 2017.

**Experiències inspiradores**
  * El nou contracte de serveis de telecomunicacions de l'Ajuntament de Barcelona incorpora mesures socials, laborals i ambientals: http://ajuntament.barcelona.cat/contractaciopublica/ca/noticia/el-nou-contracte-de-serveis-de-telecomunicacions-incorpora-mesures-socials-laborals-i-ambientals_432074


## Referències
  * Pacte Nacional per les Infrastructures: http://www.gencat.cat/especial/pni/cat/telecomunicacions.htm
  * Proposta d'Ordenança municipal per al desplegament de xarxes d'accés a serveis de telecomunicacions de nova generació i en format universal: https://fundacio.guifi.net/web/content/2322?unique=cef4bebe39b45ba50ed5ebb5e2a63ecaf07e6cb4&download=true
