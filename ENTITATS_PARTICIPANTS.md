# Entitats i persones participants

1. Impulsores:

   Els impulsors d'aquest projecte ens reunim periòdicament per debatre les aportacion fetes, parlar de les seves mancances, així com  promoure la seva projecció pública. Per demanar més informació sobre aquestes reunions pots enviar un correu a info@sobtec.cat
   * Sobtec

2. Contribuidores:

   Els contribuidors s'encarreguen de fer propostes específiques per ampliar els diferents apartats de la guia.
   Trobaràs més informació sobre com contribuir al document [COM_CONTRIBUIR](COM_CONTRIBUIR.md)

3. Simpatitzants

   Els simpatitzants donen suport i difonen els continguts de La Guia. Envia'ns un correu a info@sobtec.cat si vols aparèixer com simpatitzant de La Guia.
